package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// type ServiceAccount struct {
// 	Type         string `json:"type"`
// 	ProjectId    string `json:"project_id"`
// 	PrivateKeyId string `json:"private_key_id"`
// 	PrivateKey   string `json:"private_key"`
// 	ClientEmail  string `json:"client_email"`
// 	ClientId     string `json:"client_id"`
// 	AuthUri      string `json:"auth_uri"`
// 	TokenUri     string `json:"token_uri"`
// 	AuthProvider string `json:"auth_provider_x509_cert_url"`
// 	ClientX509   string `json:"client_x509_cert_url"`
// }
type Config struct {
	Server struct {
		Port string `yaml:"port"`
		Host string `yaml:"host"`
	} `yaml:"server"`
	Database struct {
		Username string `yaml:"user"`
		Password string `yaml:"pass"`
	} `yaml:"database"`
}

func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}

func readConfFile(cfg *Config) {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open(dirname + "/.telegram-hours-reporter-bot/config/config.yaml")
	if err != nil {
		processError(err)
	}

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
	f.Close()
}

func ReadConfiguration() Config {
	var cfg Config
	readConfFile(&cfg)
	return cfg
}

func ReadServiceAccount() string {

	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	f, err := ioutil.ReadFile(dirname + "/.telegram-hours-reporter-bot/config/service-account.json")
	if err != nil {
		log.Fatal(err)
	}

	return string(f)
}
