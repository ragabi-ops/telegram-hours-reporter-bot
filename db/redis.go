package db

import (
	"github.com/go-redis/redis"
	"gitlab.com/ragabi-ops/telegram-hours-reporter-bot/config"
)

func getDBConnection(host string, port string) redis.Client {

	client := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})

	return *client
}

func createServiceAccount() {
	println("In create")
	client := getDBConnection(config.ReadConfiguration().Server.Host, config.ReadConfiguration().Server.Port)

	err := client.Set("service-account", config.ReadServiceAccount(), 0).Err()
	if err != nil {
		panic(err)
	}
}

func GetServiceAccount() string {
	client := getDBConnection(config.ReadConfiguration().Server.Host, config.ReadConfiguration().Server.Port)
	sa, err := client.Get("service-account").Result()
	if err == redis.Nil {
		// TODO fix the recursion some how
	} else if err != nil {
		panic(err)
	}

	return sa
}
